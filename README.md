#By Valeria Ramirez
#Done on Python
#Feb 2, 2019
#Version 1.0
#For Introduction to Computer Science course
#Function: This program is a simple introduction to the language python. Prompting the user for small conversation
#and a game of "Guess the number" where users will have to enter their guess of a randomly generated number
#and the program will return a "high", "lower", or "correct" answer.